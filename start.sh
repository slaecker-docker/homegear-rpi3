#/bin/bash

_term() {
	service homegear-management stop
	service homegear stop
	exit $?
}

trap _term SIGTERM

USER=homegear

USER_ID=$(id -u $USER)
USER_GID=$(id -g $USER)

USER_ID=${HOST_USER_ID:=$USER_ID}
USER_GID=${HOST_USER_GID:=$USER_GID}

if [ $USER_ID -eq 0 ]; then
	sed -i "s/RUNASUSER=homegear/RUNASUSER=root/g" /etc/init.d/homegear
	sed -i "s/RUNASGROUP=homegear/RUNASGROUP=root/g" /etc/init.d/homegear
else
	sed -i -e "s/^${USER}:\([^:]*\):[0-9]*:[0-9]*/${USER}:\1:${USER_ID}:${USER_GID}/" /etc/passwd
	sed -i -e "s/^${USER}:\([^:]*\):[0-9]*/${USER}:\1:${USER_GID}/" /etc/group
fi

if ! [ "$(ls -A /etc/homegear)" ]; then
	cp -a /etc/homegear.config/* /etc/homegear/
fi

if ! [ "$(ls -A /var/lib/homegear)" ]; then
	cp -a /var/lib/homegear.data/* /var/lib/homegear/
else
	rm -Rf /var/lib/homegear/modules/*
	cp -a /var/lib/homegear.data/modules/* /var/lib/homegear/modules/
fi

rm -Rf /var/lib/homegear/flows/nodes/*
rm -Rf /var/lib/homegear/node-blue

if ! [ -f /var/log/homegear/homegear.log ]; then
	touch /var/log/homegear/homegear.log
fi

chown -R root:root /etc/homegear
find /etc/homegear -type d -exec chmod 755 {} \;
chown -R homegear:homegear /var/log/homegear /var/lib/homegear
find /var/log/homegear -type d -exec chmod 750 {} \;
find /var/log/homegear -type f -exec chmod 640 {} \;
find /var/lib/homegear -type d -exec chmod 750 {} \;
find /var/lib/homegear -type f -exec chmod 640 {} \;

ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

service homegear start
service homegear-management start
tail -f /var/log/homegear/homegear.log &
child=$!
wait "$child"